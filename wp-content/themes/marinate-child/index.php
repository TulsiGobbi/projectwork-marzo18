<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package marinate
 */

get_header(); 
?>

<?php if ( get_header_image() ) : ?>
	<div class="custom-header">
        <a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
            <img src="<?php header_image(); ?>" width="<?php echo absint( get_custom_header()->width ); ?>" height="<?php echo absint( get_custom_header()->height ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
        </a>
    </div>
<?php endif; ?>
       
        <section class="blog-section">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
					<form action="./wp-content/themes/travel-lite-child/mail.php" method="POST">
					<?php //if (have_posts()) : while (have_posts()) : the_post();?>

         
          <?php
		  
			if( have_posts() ) :
			$count = 1;
			 /* Start the Loop */
			 while ( have_posts() ) : the_post();
			 get_template_part( 'template-parts/content' );
			
			 if ($count % 3 == 0) {
				echo "<div class='clearfix'></div>";
			}
			$count++; 
			 endwhile;
			 
			echo "<div class='clearfix'></div>";			 
			$post_args =  array(
				'screen_reader_text' => ' ',
				'prev_text' => __( '<div class="chevronne"><i class="fa fa-chevron-left"></i></div>', 'marinate' ),
				'next_text' => __( '<div class="chevronne"><i class="fa fa-chevron-right"></i></div>', 'marinate' ),
				);
	
				the_posts_pagination( $post_args ); ?>
				<form action="./wp-content/themes/marinate-child/informazioni.php" method="POST">
						
				<?php else : ?>
					<h1 class="arc-post-title"><?php _e('Sorry, we could not find anything that matched...', 'travel-lite'); ?></h1>
					<h3 class="arc-src"><span><?php _e('You Can Try the Search...', 'travel-lite'); ?></span></h3>
					<?php get_search_form(); ?><br />
					<p><a href="<?php echo home_url(); ?>" title="<?php _e('Browse the Home Page', 'travel-lite'); ?>">&laquo; <?php _e('Or Return to the Home Page', 'travel-lite'); ?></a></p><br />
			
				<?php  get_template_part( 'template-parts/content', 'none' );?>

			<?php endif; 
	
		
		?>
                        
                        
                    </div>
                </div>
            </div>
        </section>


<!--
<input type="mail" class="form-control" placeholder="E-mail" name="mail">
<br>
-->

<input type="submit" class="btn btn-info form-control" value="INVIO">
</form>


<?php  get_footer(); ?>