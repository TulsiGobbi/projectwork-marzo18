<?php
/**
 * Template part for displaying single posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package marinate
 */

?>

            <header class="entry-header">
            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		    <div <?php post_class(); ?> id="post-<?php the_ID(); ?>">
                <h1 class="entry-title"><?php the_title(); ?></h1>                                    
            </header>
            <div class="entry-content">
                <?php the_content(); 

                    wp_link_pages( array(
                        'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'marinate' ),
                        'after'  => '</div>',
                    ) );
                ?>
                
            </div> 
