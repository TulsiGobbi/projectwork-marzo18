<?php
/**
 * Template part for displaying single posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package marinate
 */

?>

                                <header class="entry-header">
									<h1 class="entry-title"><?php the_title(); ?></h1>
                                    <?php
                                   
                                    ?>
                                    <div class="entry-meta">
                                        <span>
												<?php the_category( '&nbsp;&bull;&nbsp;' ); ?>
										</span>                                       
                                        <span class="separator"><?php echo esc_html('|', 'marinate'); ?><br></span>
                                        <span class="date"><?php //echo get_the_date(get_option( 'date-format' ) ); ?></span>
                                    </div>
                                </header>
                                <div class="entry-content">
                                <?php 
								the_content(); 
								wp_link_pages( array(
									'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'marinate' ),
									'after'  => '</div>',
								) );
								
								?>

                                <?php 
                               

                                ?>

                                <?php 
                                        //stampo periodo di apertura + funzione cambio data
                                        $periodo = get_field( "periodo_di_apertura" );
                                        echo "PERIODO APERTURA: ".DataCambio($periodo);
                                        ?>
                                        <br>
                                        <?php
                                        //stampo periodo di chiusura + funzione cambio data
                                        $periodo = get_field( "periodo_di_chiusura" );
                                        echo "PERIODO CHIUSURA: ".DataCambio($periodo);
                                        ?>
                                        <br>
                                        <?php
                                        //stampo accessibilità per disabili
                                        $accessibilita = get_field( "accessibilità_per_disabili" );
                                        echo "ACCESSIBILITA' PER DISABILI: ".$accessibilita;
                                        ?>
                                        <br>
                                        <?php
                                        //stampo prezzo d'ingresso
                                        $prezzo = get_field( "prezzo_ingresso" );
                                        echo "PREZZO INGRESSO: ".$prezzo."€";
                                        ?>
                                        <br>
                                        <?php
                                        //stampo indirizzo
                                        $indirizzo = get_field( "indirizzo" );
                                        echo "INDIRIZZO: ".$indirizzo;
                                        ?>
                                        <br>
                                        <?php
                                        //stampo slideshow
                                        $id_galleria = get_field("id_galleria");
                                        echo do_shortcode("[slideshow id=".$id_galleria."]");
                                        ?>
                                </div>

                                
								<?php the_tags( '<ul class="post-tags"><li>', '</li><li>', '</li></ul>' ); ?>		                                
