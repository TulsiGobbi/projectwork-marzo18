<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'projectworkmarzo');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'fud8+YD+b`D%/jq@Ye=uf%<1}Y-[kU~Z#(T?q=D%_v-*N_X#p1;.M+cp1MMep74K');
define('SECURE_AUTH_KEY',  '$}j*2`[pnlP)ECeNc-Fnn`e#EA|+Wh<4- e-+K.X.X?Gc%JRW1?Q`M{|I3hs}b@E');
define('LOGGED_IN_KEY',    'Ns.Km@s4{j@1a;B-V4!|lbB|p>X9I+M.-kdK-8|[~be93kwb@8&:f!l96ebe+X&q');
define('NONCE_KEY',        'OE.^0OQsZYeNE6=pGS0xAyE27n)PX7O.b8s@4G-}X6bFJP|]EbEpX=[)s@ 4bhQ]');
define('AUTH_SALT',        '1|P28f}|#15l9T#G;:wm6Z&Tg@;5^kY?NWs-HZz9p<onl?-GBJt3_:.bQe4Fvb@d');
define('SECURE_AUTH_SALT', 'z9`#?QJL<CKog.o57 5`+:g:RZ}-Z$yIj!TJn7^U[<}dM_zUZ4+VT3-GCVI)w)EL');
define('LOGGED_IN_SALT',   'V84,>%QvDsN`5?U|f!*raV_byP|M9laZ1_ @gd_!K;4;O^,G{intu>8%Ey1*lU/,');
define('NONCE_SALT',       'al vZ,({s[I)%%WOZpX,a+-|oKZjhhVC+P]#C/{3Mdkv{PlX_0G%Q>_[VwaF.SC~');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
